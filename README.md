# README #

This is the LetsLunch App for the Pragma Team Challenge.

### Developer Details ###

* Alisson Lima Selistre
* https://br.linkedin.com/in/alissonselistre

### Project Details ###

* Operational System: iOS 8.4+
* Language: Objective-C

### Extern Libraries ###

* Google Places API (information about the restaurants)
* Realm (data persistence)
* SDWebImage (image load)

### What are the highlights of your logic/code writing style? ###

* A handler for location updates considering the battery usage.
* A handler for the network calls.
* Independent methods to a better testability.
* Visited places in a separated list.
* Clean UI.
* Working offline.

### What could have been done in a better way? ###

* A custom class for the persistence to avoid the access of the Realm methods explicitly.
* Logic to indicates a restaurant from the list "To Visit".
* Logic to generates a local notification to customer before the lunch.
* Sort by distance.
* Some automated tests.
* A simple API to store the restaurant ratings to share between the clients.