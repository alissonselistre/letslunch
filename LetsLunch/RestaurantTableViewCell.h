//
//  RestaurantTableViewCell.h
//  LetsLunch
//
//  Created by Alisson L. Selistre on 12/19/16.
//  Copyright © 2016 Alisson L Selistre. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Restaurant;

@protocol RestaurantTableViewCellDelegate <NSObject>

- (void)likeButtonPressed;

@end

@interface RestaurantTableViewCell : UITableViewCell

@property (weak, nonatomic) id<RestaurantTableViewCellDelegate> delegate;

- (void)configureWithRestaurant:(Restaurant *)restaurant;

@end
