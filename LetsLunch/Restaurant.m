//
//  Restaurant.m
//  LetsLunch
//
//  Created by Alisson L. Selistre on 12/19/16.
//  Copyright © 2016 Alisson L Selistre. All rights reserved.
//

#import "Restaurant.h"
#import <CoreLocation/CoreLocation.h>

@interface Restaurant()

@property (strong, nonatomic) CLLocation *location;

@end

@implementation Restaurant

#pragma mark - Realm methods

+ (NSString *)primaryKey
{
    return @"uid";
}

+ (NSArray *)ignoredProperties
{
    return @[@"location"];
}

+ (NSDictionary *)defaultPropertyValues
{
    return @{@"rating":@0};
}

+ (void)createOrUpdateWithInfo:(NSDictionary *)info
{
    NSMutableDictionary *realmMapping = [[NSMutableDictionary alloc] init];
    
    [realmMapping setObject:[info objectForKey:@"id"] forKey:@"uid"];
    [realmMapping setObject:[info objectForKey:@"icon"] forKey:@"iconPath"];
    [realmMapping setObject:[info objectForKey:@"name"] forKey:@"name"];
    
    if ([info objectForKey:@"vicinity"])
    {
        [realmMapping setObject:[info objectForKey:@"vicinity"] forKey:@"address"];
    }
    
    NSDictionary *locationInfo = [[info objectForKey:@"geometry"] objectForKey:@"location"];
    [realmMapping setObject:[locationInfo objectForKey:@"lat"] forKey:@"latitude"];
    [realmMapping setObject:[locationInfo objectForKey:@"lng"] forKey:@"longitude"];
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [Restaurant createOrUpdateInRealm:realm withValue:realmMapping];
    [realm commitWriteTransaction];
}

#pragma mark - helpers

- (void)setNewRating:(NSInteger)newRating
{
    NSNumber *rating = @(newRating);
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    self.rating = ([rating integerValue] != [self.rating integerValue]) ? rating : @0;
    [realm commitWriteTransaction];
}

- (CLLocation *)location
{
    if (!_location && self.latitude && self.longitude)
    {
        _location = [[CLLocation alloc] initWithLatitude:[self.latitude doubleValue] longitude:[self.longitude doubleValue]];
    }
    
    return _location;
}

@end
