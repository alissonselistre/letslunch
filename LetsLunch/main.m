//
//  main.m
//  LetsLunch
//
//  Created by Alisson L. Selistre on 12/17/16.
//  Copyright © 2016 Alisson L Selistre. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
