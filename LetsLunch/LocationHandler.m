//
//  LocationHandler.m
//  LetsLunch
//
//  Created by Alisson L. Selistre on 12/19/16.
//  Copyright © 2016 Alisson L Selistre. All rights reserved.
//

#import "LocationHandler.h"

#define MIN_LOCATION_UPDATE_INTERVAL 5 // seconds

@interface LocationHandler() <CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager *locationManager;

@property (strong, nonatomic) NSDate *lastUpdate;

@end

@implementation LocationHandler

#pragma mark - singleton and custom init

+ (instancetype)sharedInstance
{
    static LocationHandler *__sharedInstance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[LocationHandler alloc] init];
    });
    
    return __sharedInstance;
}

#pragma mark - custom gets

- (CLLocationManager *)locationManager
{
    if (!_locationManager)
    {
        _locationManager = [[CLLocationManager alloc] init];
        
        [_locationManager setDelegate:self];
        [_locationManager setDistanceFilter:kCLDistanceFilterNone];
        [_locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    }
    
    return _locationManager;
}

#pragma mark - user location methods

- (void)start
{
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        [self.locationManager startUpdatingLocation];
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined)
    {
        [self.locationManager requestWhenInUseAuthorization];
    }
}

- (void)stop
{
    [self.locationManager stopUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        [self.locationManager startUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    if (locations.count)
    {
        BOOL canUpdateLocation = NO;
        
        NSDate *now = [NSDate date];
        
        if (self.lastUpdate)
        {
            NSDate *lastUpdateWithInterval = [NSDate dateWithTimeInterval:MIN_LOCATION_UPDATE_INTERVAL sinceDate:self.lastUpdate];
            canUpdateLocation = ([lastUpdateWithInterval compare:now] == NSOrderedAscending);
        }
        else
        {
            canUpdateLocation = YES;
        }
        
        if (canUpdateLocation)
        {
            self.lastUpdate = now;
            self.lastUserLocation = [locations firstObject];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:LOCATION_UPDATED_OBSERVER_KEY object:nil];
        }
    }
}

@end
