//
//  RestaurantsViewController.m
//  LetsLunch
//
//  Created by Alisson L. Selistre on 12/17/16.
//  Copyright © 2016 Alisson L Selistre. All rights reserved.
//

#import "RestaurantsViewController.h"
#import "LocationHandler.h"
#import "NetworkHandler.h"
#import "RestaurantTableViewCell.h"
#import "Restaurant.h"

@interface RestaurantsViewController() <UITableViewDataSource, UITableViewDelegate, RestaurantTableViewCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@property (strong, nonatomic) NSMutableArray *visitedRestaurants;
@property (strong, nonatomic) NSMutableArray *toVisitRestaurants;

@end

@implementation RestaurantsViewController

#pragma mark - view methods

- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[LocationHandler sharedInstance] start];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLocationUpdated) name:LOCATION_UPDATED_OBSERVER_KEY object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[LocationHandler sharedInstance] stop];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - actions

- (IBAction)segmentedControlChanged:(UISegmentedControl *)sender
{
    UITableViewRowAnimation tableViewAnimation = (sender.selectedSegmentIndex == 0) ? UITableViewRowAnimationRight : UITableViewRowAnimationLeft;
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:tableViewAnimation];
}

#pragma mark - observers

- (void)userLocationUpdated
{
    CLLocation *userLocation = [[LocationHandler sharedInstance] lastUserLocation];
    
    if (userLocation)
    {
        [NetworkHandler getRestaurantsNearLocation:userLocation withCompletionBlock:^(BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self refreshRestaurantsListAnimated:NO];
            });
        }];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.restaurants.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RestaurantTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RestaurantCellIdentifier" forIndexPath:indexPath];
    cell.delegate = self;
    
    Restaurant *restaurant = [self.restaurants objectAtIndex:indexPath.row];
    [cell configureWithRestaurant:restaurant];
    
    return cell;
}

#pragma mark - RestaurantTableViewCellDelegate

- (void)likeButtonPressed
{
    [self refreshRestaurantsListAnimated:YES];
}

#pragma mark - UI

- (void)refreshRestaurantsListAnimated:(BOOL)animated
{
    RLMResults *restaurantsFetchResult = [Restaurant allObjects];
    RLMArray *restaurants = [[RLMArray alloc] initWithObjectClassName:restaurantsFetchResult.objectClassName];
    [restaurants addObjects:restaurantsFetchResult];
    
    self.visitedRestaurants = [[NSMutableArray alloc] init];
    self.toVisitRestaurants = [[NSMutableArray alloc] init];
    
    for (Restaurant *restaurant in restaurants)
    {
        if ([restaurant.rating integerValue] > 0)
        {
            [self.visitedRestaurants addObject:restaurant];
        }
        else
        {
            [self.toVisitRestaurants addObject:restaurant];
        }
    }
    
    if (animated)
    {
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    else
    {
        [self.tableView reloadData];
    }
}

#pragma mark - helpers

- (NSMutableArray *)restaurants
{
    return (self.segmentedControl.selectedSegmentIndex == 0) ? self.toVisitRestaurants : self.visitedRestaurants;
}

@end
