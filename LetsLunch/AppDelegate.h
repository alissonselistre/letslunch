//
//  AppDelegate.h
//  LetsLunch
//
//  Created by Alisson L. Selistre on 12/17/16.
//  Copyright © 2016 Alisson L Selistre. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

