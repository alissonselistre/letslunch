//
//  Restaurant.h
//  LetsLunch
//
//  Created by Alisson L. Selistre on 12/19/16.
//  Copyright © 2016 Alisson L Selistre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@class CLLocation;

@interface Restaurant : RLMObject

@property (strong, nonatomic) NSString *uid;
@property (strong, nonatomic) NSString *iconPath;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSNumber<RLMInt> *rating;
@property (strong, nonatomic) NSNumber<RLMDouble> *latitude;
@property (strong, nonatomic) NSNumber<RLMDouble> *longitude;

+ (void)createOrUpdateWithInfo:(NSDictionary *)info;

- (void)setNewRating:(NSInteger)newRating;

- (CLLocation *)location;

@end
