//
//  NetworkHandler.m
//  LetsLunch
//
//  Created by Alisson L. Selistre on 12/19/16.
//  Copyright © 2016 Alisson L Selistre. All rights reserved.
//

#import "NetworkHandler.h"
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "Restaurant.h"

// Configuration
#define kGoogleAPIKey @"AIzaSyC_O46ta4wK03Lvd7ywYxVqvurvhVrTBS4"
#define kDefaultRadius 5000

@implementation NetworkHandler

+ (void)getRestaurantsNearLocation:(CLLocation *)location withCompletionBlock:(restaurantsCompletionBlock)completion
{
    NSString *requestString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/search/json?location=%f,%f&radius=%d&types=restaurant&sensor=true&key=%@", location.coordinate.latitude, location.coordinate.longitude, kDefaultRadius, kGoogleAPIKey];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:requestString] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSError *jsonError;
        
        if (data)
        {
            id responseObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
            
            if (!jsonError && responseObject)
            {
                NSDictionary *responseDic = responseObject;
                
                NSArray *results = [responseDic objectForKey:@"results"];
                if (results)
                {
                    for (NSDictionary *restaurantInfo in results)
                    {
                        [Restaurant createOrUpdateWithInfo:restaurantInfo];
                    }
                    
                    completion(YES);
                    return;
                }
            }
        }
        
        completion(NO);
        
    }] resume];
}

@end
