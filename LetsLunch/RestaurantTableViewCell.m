//
//  RestaurantTableViewCell.m
//  LetsLunch
//
//  Created by Alisson L. Selistre on 12/19/16.
//  Copyright © 2016 Alisson L Selistre. All rights reserved.
//

#import "RestaurantTableViewCell.h"
#import "LocationHandler.h"
#import "Restaurant.h"
#import "SDWebImage/UIImageView+WebCache.h"

@interface RestaurantTableViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;

@property (weak, nonatomic) IBOutlet UIButton *likeButton1;
@property (weak, nonatomic) IBOutlet UIButton *likeButton2;
@property (weak, nonatomic) IBOutlet UIButton *likeButton3;
@property (weak, nonatomic) IBOutlet UIButton *likeButton4;
@property (weak, nonatomic) IBOutlet UIButton *likeButton5;

@property (strong, nonatomic) NSArray *likeButtons;

@property (strong, nonatomic) Restaurant *restaurant;

@property (strong, nonatomic) NSNumberFormatter *ratingFormatter;

@end

@implementation RestaurantTableViewCell

#pragma mark - custom gets

- (NSNumberFormatter *)ratingFormatter
{
    if (!_ratingFormatter)
    {
        _ratingFormatter = [[NSNumberFormatter alloc] init];
        [_ratingFormatter setMaximumFractionDigits:1];
    }
    return _ratingFormatter;
}

#pragma mark - layout

- (void)awakeFromNib
{
    [super awakeFromNib];

    [self organizeLikeButtons];
    [self layoutForFirstState];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    [self layoutForFirstState];
}

- (void)layoutForFirstState
{
    self.iconImageView.image = [UIImage imageNamed:@"waiter"];
    
    for (UIButton *likeButton in self.likeButtons)
    {
        [likeButton setImage:[UIImage imageNamed:@"like-gray"] forState:UIControlStateNormal];
    }
    
    self.titleLabel.text = nil;
    self.addressLabel.text = nil;
    self.distanceLabel.text = nil;
}

#pragma mark - UI setup

- (void)configureWithRestaurant:(Restaurant *)restaurant
{
    self.restaurant = restaurant;
    
    self.titleLabel.text = restaurant.name;
    self.addressLabel.text = restaurant.address;
    
    if (restaurant.location)
    {
        CLLocationDistance distance = [[[LocationHandler sharedInstance] lastUserLocation] distanceFromLocation:restaurant.location];
        self.distanceLabel.text = [self stringFromDistance:distance];
    }

    if (restaurant.iconPath.length)
    {
        [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:restaurant.iconPath] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {

            self.iconImageView.image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [self.iconImageView setTintColor:[UIColor whiteColor]];
        }];
    }
    
    [self configureLikeButtonsWithRating:[restaurant.rating integerValue]];
}

- (void)configureLikeButtonsWithRating:(NSInteger)rating
{
    for (int i=0; i < rating; i++)
    {
        UIButton *likeButton = [self.likeButtons objectAtIndex:i];
        [likeButton setImage:[UIImage imageNamed:@"like-pink"] forState:UIControlStateNormal];
    }
}

#pragma mark - actions

- (IBAction)likeButtonPressed:(UIButton *)sender
{
    [self.restaurant setNewRating:sender.tag];
    
    if ([self.delegate respondsToSelector:@selector(likeButtonPressed)])
    {
        [self.delegate likeButtonPressed];
    }
}

#pragma mark - helpers

- (void)organizeLikeButtons
{
    self.likeButtons = @[self.likeButton1, self.likeButton2, self.likeButton3, self.likeButton4, self.likeButton5];
}

- (NSString *)stringFromDistance:(CLLocationDistance)distance
{
    NSString *formattedString;
    
    if (distance >= 1000)
    {
        distance = distance/1000;
        formattedString = [NSString stringWithFormat:@"%.1fkm away", distance];
    }
    else
    {
        formattedString = [NSString stringWithFormat:@"%.0fm away", distance];
    }
    
    return formattedString;
}

@end
