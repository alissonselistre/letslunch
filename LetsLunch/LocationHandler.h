//
//  LocationHandler.h
//  LetsLunch
//
//  Created by Alisson L. Selistre on 12/19/16.
//  Copyright © 2016 Alisson L Selistre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#define LOCATION_UPDATED_OBSERVER_KEY @"UserLocationUpdated"

@interface LocationHandler : NSObject

+ (instancetype)sharedInstance;

/**
 * Start to update the user location. This method handles the necessary permissions to use the GPS if needed.
 */
- (void)start;

/**
 * Stop to update the user location.
 */
- (void)stop;

/**
 * The last user location based in the device's GPS.
 */
@property (strong, nonatomic) CLLocation *lastUserLocation;

@end
