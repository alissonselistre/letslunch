//
//  NetworkHandler.h
//  LetsLunch
//
//  Created by Alisson L. Selistre on 12/19/16.
//  Copyright © 2016 Alisson L Selistre. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CLLocation;

@interface NetworkHandler : NSObject

typedef void(^restaurantsCompletionBlock)(BOOL success);

/**
 * Requests a list of Restaurant objects based in the given location.
 *
 * @param location a CLLocation to find the nearest restaurants.
 */
+ (void)getRestaurantsNearLocation:(CLLocation *)location withCompletionBlock:(restaurantsCompletionBlock)completion;

@end
